import {Component, OnInit} from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import {Observable} from "rxjs";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'foe-buildings-webapp';
  items: Observable<any[]>;
  constructor(private fireStore: AngularFirestore) {
  }

  ngOnInit() {
    this.items = this.fireStore.collection("productionBuildings").valueChanges();
  }

  stringifyItem(item) {
    console.debug(item);
    return JSON.stringify(item);
  }
}
