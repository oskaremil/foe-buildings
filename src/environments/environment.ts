// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

// noinspection SpellCheckingInspection
export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBcrvW0zII1w4y0WMLo53MHtTQqKBn8Q_w",
    authDomain: "foe-buildings.firebaseapp.com",
    databaseURL: "https://foe-buildings.firebaseio.com",
    projectId: "foe-buildings",
    storageBucket: "foe-buildings.appspot.com",
    messagingSenderId: "55820674017",
    appId: "1:55820674017:web:ee300ae12d015be9"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
